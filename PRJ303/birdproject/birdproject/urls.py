from django.contrib import admin
from django.urls import path,include
from birdapp import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('birdapp.urls')),
    # path('', views.home, name='home'),
    # path('register/', views.register, name='register'),
    # path('login/', views.user_login, name='login'),
    # path('logout/', views.logout_view, name='logout'),
    # path('homediscover/', views.homediscover, name='homediscover'),
    # path('BirdDetail/', views.BirdDetail, name='BirdDetail'),

    # # User Dashboard URLs
    # path('userhome/', views.userhome, name='userhome'),
    # path('userhomediscover/', views.userhomediscover, name='userhomediscover'),
]
