bird_details = {
    "ABBOTTS BABBLER": {
        "scientific_name": "Malacocincla abbotti",
        "family": "Timaliidae",
        "description": "The Abbott's babbler is a small passerine bird found in Southeast Asia, known for its distinctive plumage and song."
    },
    "ALEXANDRINE PARAKEET": {
        "scientific_name": "Psittacula eupatria",
        "family": "Psittacidae",
        "description": "The Alexandrine parakeet is a large parrot species native to the Indian subcontinent and Southeast Asia, characterized by its bright plumage and ringed neck."
    },
    "ASIAN OPENBILL STORK": {
        "scientific_name": "Anastomus oscitans",
        "family": "Ciconiidae",
        "description": "The Asian openbill stork is a large wading bird with a distinctive beak characterized by the gap between its upper and lower mandibles."
    },
    "BAIKAL TEAL": {
        "scientific_name": "Sibirionetta formosa",
        "family": "Anatidae",
        "description": "The Baikal teal is a small dabbling duck that breeds in Siberia and migrates to East Asia during winter."
    },
    "BARN SWALLOW": {
        "scientific_name": "Hirundo rustica",
        "family": "Hirundinidae",
        "description": "The barn swallow is a medium-sized bird known for its long tail streamers and agile flight, often found near human settlements."
    },
    "BLACK BAZA": {
        "scientific_name": "Aviceda leuphotes",
        "family": "Accipitridae",
        "description": "The black baza is a small raptor found in Southeast Asia, known for its dark plumage and aerial hunting abilities."
    },
    "BLACK FRANCOLIN": {
        "scientific_name": "Francolinus francolinus",
        "family": "Phasianidae",
        "description": "The black francolin is a gamebird species native to South Asia, characterized by its black plumage and distinctive call."
    },
    "BLOOD PHEASANT": {
        "scientific_name": "Ithaginis cruentus",
        "family": "Phasianidae",
        "description": "The blood pheasant is a bird species found in the eastern Himalayas, known for its vibrant plumage and red facial patches."
    },
    "CHESTNUT WINGED CUCKOO": {
        "scientific_name": "Clamator coromandus",
        "family": "Cuculidae",
        "description": "The chestnut-winged cuckoo is a large cuckoo species found in South and Southeast Asia, characterized by its distinctive chestnut wings and long tail."
    },
    "CHINESE POND HERON": {
        "scientific_name": "Ardeola bacchus",
        "family": "Ardeidae",
        "description": "The Chinese pond heron is a small heron species found in East Asia, known for its colorful breeding plumage and its habit of standing still to hunt prey."
    },
    "COMMON IORA": {
        "scientific_name": "Aegithina tiphia",
        "family": "Aegithinidae",
        "description": "The common iora is a small passerine bird found in South and Southeast Asia, known for its bright yellow plumage and insectivorous diet."
    },
    "COPPERSMITH BARBET": {
        "scientific_name": "Psilopogon haemacephalus",
        "family": "Megalaimidae",
        "description": "The coppersmith barbet is a bird species found in the Indian subcontinent and Southeast Asia, known for its distinctive call and colorful plumage."
    },
    "CRESTED SERPENT EAGLE": {
        "scientific_name": "Spilornis cheela",
        "family": "Accipitridae",
        "description": "The crested serpent eagle is a medium-sized bird of prey found in South and Southeast Asia, characterized by its prominent crest and serpent-like appearance."
    },
    "CRIMSON SUNBIRD": {
        "scientific_name": "Aethopyga siparaja",
        "family": "Nectariniidae",
        "description": "The crimson sunbird is a small passerine bird found in South and Southeast Asia, known for its vibrant red plumage and long, curved bill."
    },
    "DARJEELING WOODPECKER": {
        "scientific_name": "Dendrocopos darjellensis",
        "family": "Picidae",
        "description": "The Darjeeling woodpecker is a bird species found in the eastern Himalayas, known for its black and yellow plumage and its habit of drilling holes in tree bark."
    },
    "DEMOISELLE CRANE": {
        "scientific_name": "Grus virgo",
        "family": "Gruidae",
        "description": "The demoiselle crane is a species of crane found in Central Asia, known for its graceful appearance and elaborate courtship dances."
    },
    "EURASIAN MAGPIE": {
        "scientific_name": "Pica pica",
        "family": "Corvidae",
        "description": "The Eurasian magpie is a medium-sized bird of the crow family, known for its black and white plumage and its habit of collecting shiny objects."
    },
    "FOREST WAGTAIL": {
        "scientific_name": "Dendronanthus indicus",
        "family": "Motacillidae",
        "description": "The forest wagtail is a small passerine bird found in Asia, known for its distinctive long tail and its habit of wagging it up and down."
    },
    "GOLDEN EAGLE": {
        "scientific_name": "Aquila chrysaetos",
        "family": "Accipitridae",
        "description": "The golden eagle is a large bird of prey found in North America, Europe, and Asia, known for its powerful build and golden-brown plumage."
    },
    "GRANDALA": {
        "scientific_name": "Grandala coelicolor",
        "family": "Turdidae",
        "description": "The grandala is a bird species found in the high altitudes of the Himalayas, characterized by its stunning blue plumage and its preference for open areas."
    },
    "GREEN MAGPIE": {
        "scientific_name": "Cissa chinensis",
        "family": "Corvidae",
        "description": "The green magpie is a striking bird found in the forests of East Asia, known for its vivid green plumage and long, graduated tail."
    },
    "HIMALAYAN MONAL": {
        "scientific_name": "Lophophorus impejanus",
        "family": "Phasianidae",
        "description": "The Himalayan monal is a strikingly colorful bird found in the high altitudes of the Himalayas."
    },
    "HOOPOES": {
        "scientific_name": "Upupa epops",
        "family": "Upupidae",
        "description": "The hoopoe is a bird species found across Afro-Eurasia, known for its distinctive crown of feathers and its habit of probing the ground for insects with its long bill."
    },
    "HORNED LARK": {
        "scientific_name": "Eremophila alpestris",
        "family": "Alaudidae",
        "description": "The horned lark is a small songbird found in North America and Eurasia, known for its distinct black face mask and the feather tufts on its head."
    },
    "HOUSE SPARROW": {
        "scientific_name": "Passer domesticus",
        "family": "Passeridae",
        "description": "The house sparrow is a small passerine bird found worldwide, often associated with human settlements."
    },
    "IBISBILL": {
        "scientific_name": "Ibidorhyncha struthersii",
        "family": "Ibidorhynchidae",
        "description": "The ibisbill is a unique wading bird known for its long, down-curved bill and distinct plumage."
    },
    "INDIAN PITTA": {
        "scientific_name": "Pitta brachyura",
        "family": "Pittidae",
        "description": "The Indian pitta is a small passerine bird found in the Indian subcontinent and Southeast Asia, known for its vibrant plumage and its habit of hopping on the forest floor."
    },
    "INDIAN ROLLER": {
        "scientific_name": "Coracias benghalensis",
        "family": "Coraciidae",
        "description": "The Indian roller is a medium-sized bird found in South Asia, known for its bright blue plumage and acrobatic aerial displays during courtship."
    },
    "LESSER ADJUTANT": {
        "scientific_name": "Leptoptilos javanicus",
        "family": "Ciconiidae",
        "description": "The lesser adjutant is a large stork species found in South and Southeast Asia, characterized by its massive bill and distinctive hunched posture."
    },
    "LONG-EARED OWL": {
        "scientific_name": "Asio otus",
        "family": "Strigidae",
        "description": "The long-eared owl is a medium-sized owl species found in Europe, Asia, and North America, known for its prominent ear tufts and its habit of roosting in dense foliage."
    },
    "MALLARD DUCK": {
        "scientific_name": "Anas platyrhynchos",
        "family": "Anatidae",
        "description": "The mallard duck is a medium-sized dabbling duck found in North America, Europe, and Asia, characterized by the male's green head and the female's mottled brown plumage."
    },
    "MANDRIN DUCK": {
        "scientific_name": "Aix galericulata",
        "family": "Anatidae",
        "description": "The mandarin duck is a colorful waterfowl species native to East Asia, known for the male's elaborate plumage and the female's more subdued appearance."
    },
    "MERLIN": {
        "scientific_name": "Falco columbarius",
        "family": "Falconidae",
        "description": "The merlin is a small falcon species found in North America, Europe, and Asia, known for its swift flight and hunting skills."
    },
    "NICOBAR PIGEON": {
        "scientific_name": "Caloenas nicobarica",
        "family": "Columbidae",
        "description": "The Nicobar pigeon is a large pigeon species found in the Nicobar Islands and parts of Southeast Asia, known for its colorful plumage and iridescent feathers."
    },
    "NORTHERN GOSHAWK": {
        "scientific_name": "Accipiter gentilis",
        "family": "Accipitridae",
        "description": "The northern goshawk is a powerful raptor found in North America, Europe, and Asia, characterized by its broad wings and fierce hunting behavior."
    },
    "ORANGE-BELLIED LEAFBIRD": {
        "scientific_name": "Chloropsis hardwickii",
        "family": "Chloropseidae",
        "description": "The orange-bellied leafbird is a small passerine bird found in the Indian subcontinent and Southeast Asia, known for its vibrant green plumage and orange belly."
    },
    "OSPREY": {
        "scientific_name": "Pandion haliaetus",
        "family": "Pandionidae",
        "description": "The osprey is a large raptor found worldwide, known for its ability to dive into water to catch fish."
    },
    "PAINTED STORK": {
        "scientific_name": "Mycteria leucocephala",
        "family": "Ciconiidae",
        "description": "The painted stork is a large wading bird found in South Asia and Southeast Asia, characterized by its pink-banded wings and yellow beak."
    },
    "PIED CUCKOO": {
        "scientific_name": "Clamator jacobinus",
        "family": "Cuculidae",
        "description": "The pied cuckoo is a medium-sized cuckoo species found in South Asia, known for its black and white plumage and its habit of parasitizing the nests of other bird species."
    },
    "PURPLE SWAMPHEN": {
        "scientific_name": "Porphyrio porphyrio",
        "family": "Rallidae",
        "description": "The purple swamphen is a large bird species found in wetlands of Europe, Asia, and Africa, known for its vibrant purple-blue plumage and long toes adapted for walking on floating vegetation."
    },
    "RED-BILLED BLUE MAGPIE": {
        "scientific_name": "Urocissa erythroryncha",
        "family": "Corvidae",
        "description": "The red-billed blue magpie is a colorful bird found in the Himalayas and parts of East Asia, known for its blue and white plumage and its habit of collecting shiny objects."
    },
    "RED-BILLED LEIOTHRIX": {
        "scientific_name": "Leiothrix lutea",
        "family": "Leiothrichidae",
        "description": "The red-billed leiothrix is a small passerine bird native to the Indian subcontinent and Southeast Asia, known for its bright red beak and vibrant plumage."
    },
    "RED JUNGLEFOWL": {
        "scientific_name": "Gallus gallus",
        "family": "Phasianidae",
        "description": "The red junglefowl is a wild ancestor of the domestic chicken, found in South and Southeast Asia, known for its red plumage and distinctive crowing call."
    },
    "RED-VENTED BULBUL": {
        "scientific_name": "Pycnonotus cafer",
        "family": "Pycnonotidae",
        "description": "The red-vented bulbul is a medium-sized songbird found in South Asia, known for its brown plumage, red vent, and melodious calls."
    },
    "RUFOUS-BELLIED NILTAVA": {
        "scientific_name": "Niltava sundara",
        "family": "Muscicapidae",
        "description": "The rufous-bellied niltava is a small bird found in the eastern Himalayas and Southeast Asia, known for its rufous belly and striking blue upperparts."
    },
    "RUFOUS HORNBILL": {
        "scientific_name": "Buceros hydrocorax",
        "family": "Bucerotidae",
        "description": "The rufous hornbill is a large bird found in Southeast Asia, known for its distinctive reddish-brown plumage and large curved bill."
    },
    "RUFOUS WOODPECKER": {
        "scientific_name": "Micropternus brachyurus",
        "family": "Picidae",
        "description": "The rufous woodpecker is a medium-sized woodpecker species found in South and Southeast Asia, known for its reddish-brown plumage and its habit of drumming on tree trunks."
    },
    "SARUS CRANE": {
        "scientific_name": "Grus antigone",
        "family": "Gruidae",
        "description": "The sarus crane is a large crane species found in parts of South Asia and Southeast Asia, known for its tall stature and red head and upper neck."
    },
    "SCARLET MINIVET": {
        "scientific_name": "Pericrocotus flammeus",
        "family": "Campephagidae",
        "description": "The scarlet minivet is a small passerine bird found in the Indian subcontinent and Southeast Asia, known for the striking contrast between the male's bright red plumage and the female's gray and yellow colors."
    },
    "SCARLET TANAGER": {
        "scientific_name": "Piranga olivacea",
        "family": "Cardinalidae",
        "description": "The scarlet tanager is a medium-sized songbird found in North America, known for the male's vibrant red plumage and the female's yellow-green coloration."
    },
    "SIAMESE FIREBACK": {
        "scientific_name": "Lophura diardi",
        "family": "Phasianidae",
        "description": "The Siamese fireback is a medium-sized pheasant species found in Southeast Asia, known for the male's striking plumage with fiery red and black patterns."
    },
    "SIBERIAN RUBYTHROAT": {
        "scientific_name": "Luscinia calliope",
        "family": "Muscicapidae",
        "description": "The Siberian rubythroat is a small passerine bird found in northern Asia, known for the male's bright red throat and its melodic song."
    },
    "SILVER-EARED MESIA": {
        "scientific_name": "Leiothrix argentauris",
        "family": "Leiothrichidae",
        "description": "The silver-eared mesia is a small passerine bird found in the eastern Himalayas and Southeast Asia, known for its black head, bright red bill, and silver ear patches."
    },
    "SNOWY OWL": {
        "scientific_name": "Bubo scandiacus",
        "family": "Strigidae",
        "description": "The snowy owl is a large owl species found in the Arctic regions of North America and Eurasia, known for its white plumage and piercing yellow eyes."
    },
    "SPOON-BILLED SANDPIPER": {
        "scientific_name": "Eurynorhynchus pygmeus",
        "family": "Scolopacidae",
        "description": "The spoon-billed sandpiper is a critically endangered bird species found in Northeast Asia, known for its unique spoon-shaped bill and its habit of feeding on small invertebrates along coastal areas."
    },
    "SRI LANKA JUNGLEFOWL": {
        "scientific_name": "Gallus lafayettii",
        "family": "Phasianidae",
        "description": "The Sri Lanka junglefowl is a bird species endemic to Sri Lanka, closely related to the red junglefowl, with males sporting brightly colored plumage and long, curved tail feathers."
    },
    "STEERE'S LIOCICHLA": {
        "scientific_name": "Liocichla steerii",
        "family": "Leiothrichidae",
        "description": "Steere's liocichla is a medium-sized passerine bird found in the mountainous regions of Southeast Asia, known for its vibrant plumage featuring shades of blue, green, and yellow."
    },
    "SULTAN TIT": {
        "scientific_name": "Melanochlora sultanea",
        "family": "Paridae",
        "description": "The sultan tit is a small songbird found in the Himalayan region and parts of Southeast Asia, known for its black and yellow plumage and its crested head."
    },
    "SUNBIRD": {
        "scientific_name": "Nectarinia",
        "family": "Nectariniidae",
        "description": "Sunbirds are small, brightly colored birds found in Africa and Asia. They are known for their slender bills, vibrant plumage, and their ability to feed on nectar."
    },
    "TAIWAN MAGPIE": {
        "scientific_name": "Urocissa caerulea",
        "family": "Corvidae",
        "description": "The Taiwan magpie is a bird species endemic to Taiwan, known for its glossy black plumage, long tail, and striking blue markings on its wings."
    },
    "VIOLET CUCKOO": {
        "scientific_name": "Chrysococcyx xanthorhynchus",
        "family": "Cuculidae",
        "description": "The violet cuckoo is a medium-sized cuckoo species found in South and Southeast Asia, known for the male's violet plumage and the female's brown coloration."
    },
    "WHITE-BREASTED KINGFISHER": {
        "scientific_name": "Halcyon smyrnensis",
        "family": "Alcedinidae",
        "description": "The white-breasted kingfisher is a large kingfisher species found in Asia and parts of Europe, known for its blue wings, white breast, and distinctive long beak."
    },
    "WHITE-BELLIED WOODPECKER": {
        "scientific_name": "Dryocopus javensis",
        "family": "Picidae",
        "description": "The white-bellied woodpecker is a large woodpecker species found in Southeast Asia, known for its black and white plumage and its habit of drilling tree trunks in search of insects."
    },
    "WHITE-BROWED SHORTWING": {
        "scientific_name": "Brachypteryx montana",
        "family": "Turdidae",
        "description": "The white-browed shortwing is a small thrush species found in the Himalayas and parts of Southeast Asia, known for its brown plumage, white eyebrow, and melodious song."
    },
    "WHITE-CROWNED FORKTAIL": {
        "scientific_name": "Enicurus leschenaulti",
        "family": "Muscicapidae",
        "description": "The white-crowned forktail is a small bird species found in South and Southeast Asia, known for its black and white plumage and its habit of wagging its tail up and down."
    },
    "WHITE-HEADED BULBUL": {
        "scientific_name": "Pycnonotus dispar",
        "family": "Pycnonotidae",
        "description": "The white-headed bulbul is a medium-sized songbird found in Southeast Asia, known for its black and white plumage and its melodic calls."
    },
    "WHITE-NECKED JACOBIN": {
        "scientific_name": "Florisuga mellivora",
        "family": "Trochilidae",
        "description": "The white-necked jacobin is a hummingbird species found in Central and South America, known for the male's striking plumage with a white collar and the female's green and white coloration."
    },
    "WHITE-RUMPED SHAMA": {
        "scientific_name": "Copsychus malabaricus",
        "family": "Muscicapidae",
        "description": "The white-rumped shama is a songbird species found in the Indian subcontinent and Southeast Asia, known for the male's black plumage with a white rump and the female's brown coloration."
    },
    "YELLOW-BELLIED FLOWERPECKER": {
        "scientific_name": "Dicaeum melanoxanthum",
        "family": "Dicaeidae",
        "description": "The yellow-bellied flowerpecker is a small bird species found in Southeast Asia, known for its olive-green plumage, yellow belly, and its feeding on nectar and small fruits."
    },
    "YELLOW-BREASTED BUNTING": {
        "scientific_name": "Emberiza aureola",
        "family": "Emberizidae",
        "description": "The yellow-breasted bunting is a small passerine bird found in Eurasia, known for the male's bright yellow underparts and the female's more subdued plumage."
    },
    "YELLOW-BREASTED FLOWERPECKER": {
        "scientific_name": "Prionochilus maculatus",
        "family": "Dicaeidae",
        "description": "The yellow-breasted flowerpecker is a small bird species found in Southeast Asia, known for the male's yellow underparts, black and yellow upperparts, and its feeding on nectar and small fruits."
    },
    "YELLOW-THROATED FLOWERPECKER": {
        "scientific_name": "Dicaeum melanoxanthum",
        "family": "Dicaeidae",
        "description": "The yellow-throated flowerpecker is a small bird species found in Southeast Asia, known for the male's black and yellow plumage, yellow throat, and its feeding on nectar and small fruits."
    },
    "YELLOW-VENTED FLOWERPECKER": {
        "scientific_name": "Dicaeum chrysorrheum",
        "family": "Dicaeidae",
        "description": "The yellow-vented flowerpecker is a small bird species found in Southeast Asia, known for its greenish plumage, yellow vent, and its feeding on nectar and small fruits."
    }
}
