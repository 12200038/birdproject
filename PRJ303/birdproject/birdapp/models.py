from django.db import models
from django.contrib.auth.models import User

class Feedback(models.Model):
    user= models.CharField(max_length=15)
    name = models.CharField(max_length=15)
    email = models.EmailField()
    subject = models.CharField(max_length=255)
    message = models.TextField()

    def __str__(self):
        return self.subject


class Bird(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    predicted_class = models.CharField(max_length=100)
    probability = models.FloatField()
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return (self.user.username)
