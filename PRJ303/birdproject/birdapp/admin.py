from django.contrib import admin
from .models import Bird,Feedback
# Register your models here.


class BirdAdmin(admin.ModelAdmin):
    list_display = ('user','predicted_class','predicted_class','probability','date_created')

admin.site.register(Bird,BirdAdmin)
admin.site.register(Feedback)

