from django.shortcuts import render ,redirect,get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.db import IntegrityError
from .forms import FeedbackForm
from .models import Feedback,Bird
from .class_names import class_names
from django.core.files.storage import FileSystemStorage
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image
from django.contrib import messages
import numpy as np
import os,re,base64
from .bird_data import bird_details

def home(request):
    return render(request, 'home.html')

@login_required
def userhome(request):
    birds = Bird.objects.filter(user=request.user)
    form = FeedbackForm()

    if request.method == 'POST':
        if 'feedback' in request.POST:
            form = FeedbackForm(request.POST)
            if form.is_valid():
                name = form.cleaned_data['name']
                email = form.cleaned_data['email']
                subject = form.cleaned_data['subject']
                message = form.cleaned_data['message']
                user = request.user
                feedback = Feedback(user=user, name=name, email=email, subject=subject, message=message)
                feedback.save()
                messages.success(request, "Feedback submitted successful.")
                return redirect('/userhome#contact')

        if 'bird_id' in request.POST:
            bird_id = request.POST.get('bird_id')
            bird = get_object_or_404(Bird, id=bird_id)
            if 'confirm_delete' in request.POST:
                bird.delete()
                return redirect('/userhome#profile')


    context = {'birds': birds, 'form': form}
    return render(request, 'userhome.html', context)


@csrf_exempt
def register(request):
    if request.method == 'POST':
        uname = request.POST.get('username')
        email = request.POST.get('email')
        pass1 = request.POST.get('password1')
        pass2 = request.POST.get('password2')
        
        # validate username
        if not uname:
            error_message = "Please enter a username."
        elif User.objects.filter(username=uname).exists():
            error_message = "Username already exists."
        elif len(uname) < 3:
            error_message = "Username must be at least 3 characters long."
        elif len(uname) > 30:
            error_message = "Username can be at most 30 characters long."
        elif not re.match("^[a-zA-Z ]+$", uname):
            error_message = "Username should contain only character."    
    
        else:
            # validate email
            if not email:
                error_message = "Please enter an email."
            elif User.objects.filter(email=email).exists():
                error_message = "Email already exists."
           
            else:
                # validate password
                if not pass1:
                    error_message = "Please enter a password."
                elif pass1 != pass2:
                    error_message = "Password and confirm password do not match."
                else:
                    # create user
                    try:
                        my_user = User.objects.create_user(email=email, password=pass1, username=uname)
                        my_user.save()
                        messages.success(request, "Registration successful. You can now login.")
                        return redirect('register')
                    except IntegrityError:
                        error_message = "An error occurred while creating your account. Please try again later."
        
        context = {
            'error_message': error_message,
            'uname': uname,
            'email': email,
        }
        return render(request, 'register.html', context)

    return render(request, 'register.html')

def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('pass')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('userhome')
        else:
            context = {'error': 'Username or Password is incorrect!!!'}
            return render(request, 'login.html', context)

    return render(request, 'login.html')


def logout_view(request):
    logout(request)
    return redirect('home')


MODEL_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'Birdmodel.h5')

# Load the trained model
model = load_model(MODEL_PATH)

def predict_species(image_path):
    # Load the image and preprocess it
    img = image.load_img(image_path, target_size=(224, 224))
    img_array = image.img_to_array(img)
    img_array = np.expand_dims(img_array, axis=0)
    img_array /= 255.

    # Make the prediction
    prediction = model.predict(img_array)

    # Get the predicted class name and probability
    predicted_class = class_names[np.argmax(prediction)]
    probability = round(np.max(prediction) * 100, 2)

    return predicted_class, probability




def BirdDetail(request):
    return render(request, 'BirdDetail.html')



def homediscover(request):
    context = {}
    if request.method == 'POST':
        uploaded_file = request.FILES.get('image')
        if not uploaded_file:
            context['error_message'] = '* No image selected for prediction.'
        else:
            fs = FileSystemStorage()
            image_path = fs.save(uploaded_file.name, uploaded_file)
            predicted_class, probability = predict_species(image_path)

            bird_info = bird_details.get(predicted_class)
            # if bird_info:
            with open(image_path, "rb") as f:
                encoded_image = base64.b64encode(f.read()).decode("utf-8")
            fs.delete(image_path)

            context['encoded_image'] = encoded_image
            context['predicted_class'] = predicted_class
            context['probability'] = probability
            context['bird_info'] = bird_info
            # else:
            #     fs.delete(image_path)
            #     context['error_message'] = 'This is bird species is unknown.'
    
    return render(request, 'homediscover.html', context=context)

@login_required
def userhomediscover(request):
    context = {}
    if request.method == 'POST':
        uploaded_file = request.FILES.get('image')
        if not uploaded_file:
            context['error_message'] = '* No image selected for prediction.'
        else:
            fs = FileSystemStorage()
            image_path = fs.save(uploaded_file.name, uploaded_file)
            predicted_class, probability = predict_species(image_path)
        
            bird_info = bird_details.get(predicted_class)
            if bird_info:
                with open(image_path, "rb") as f:
                    encoded_image = base64.b64encode(f.read()).decode("utf-8")
                fs.delete(image_path)

            bird = Bird(
                user=request.user,
                predicted_class=predicted_class,
                probability=probability,
            )
            bird.save()

            context['encoded_image'] = encoded_image
            context['predicted_class'] = predicted_class
            context['probability'] = probability
            context['bird_info'] = bird_info


    return render(request, 'userhomediscover.html', context=context)





























































































































































































































































































































































































































































































































































