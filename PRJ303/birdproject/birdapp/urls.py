from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('register/', views.register, name='register'),
    path('login/', views.user_login, name='login'),
    path('userhome/', views.userhome, name='userhome'),
    path('logout/', views.logout_view, name='logout'),
    path('homediscover/', views.homediscover, name='homediscover'),
    path('userhomediscover/', views.userhomediscover, name='userhomediscover'),
    path('BirdDetail/', views.BirdDetail, name='BirdDetail'),
]
